## The Job
A. About us  
* Skylab Innogram Vietnam operates in IT industry. We provide an advanced network, data gathering, data delivery services that will empower our customers and partners.  

* “Design an end-to-end ecosystem that can and will drive synergies and network effects through improved Security, Latency & Scalability to address key industry challenges…”  
* Our page: www.skylabteam.com  

B. Job Description  

* Design & Developing Front-End for company's key product, including highly interactive UI for configuration, monitoring and reporting in IoT industry. You will working directly with Product Manger and CTO, for getting requirements, and bring the ideas to real world, using your skills and imagination.  

C. Your Skills and Experience  
Required experience:  

* 2+ years experience in developing web applications  
* Advanced knowledge in HTML5, CSS3 and JavaScript  
* Understanding of CSS preprocessors (SASS, LESS, Stylus)  
* Knowledge of CSS Frameworks (e.g. Bootstrap, Foundation)  
* Hands-on experience in developing web applications as a team (e.g. SCRUM for agile software development, Git as version control)  
* Familiarity with responsive web development and mobile optimisation  
* Good English communication skills (verbal and written)  

Big Bonus  

* Experience with JavaScript Frameworks like React.js (with Redux or Flux), React Native, Angular.js, Ember.js, Backbone.js, ...  
* Experience with ECMAScript 6, CoffeeScript, TypeScript, Node.js, …  
* Familiarity with HTTP or REST  
* Knowledge of Frontend Build Tools (e.g. Webpack, Gulp, Grunt, Bower, Yeoman)  
* Understanding of cross-browser and cross-device compatibility issues  
* Unit testing (e.g. with Karma, Mocha, Jasmine, enzyme)  
* Engagement in any kind of open source projects, events or other individual training  

D. Why You'll Love Working Here  
* Fun- Join a young and dynamic team in an international, professional and English speaking working environment  
* Tech Rockstar- Apply the latest technology  
* Exciting Projects- Working with oversea clients and awesome projects  
* Friendly - Open and honest culture where people are valued, treated fairly, trusted and empowered to do great things.  

E. Benefits include:  

* Macbook Pro is waiting for you  
* Year end bonus (13th month salary ++ and much more)  
* 40 hour work weeks (Mon-Fri)  
* 15 days annual leaves  
* Salary review 1 times/ year  
* Private Medical Insurance  
* Company trip , team building events monthly, etc  

## For applying, please send your CV to:  ha.tran@innogram.co

